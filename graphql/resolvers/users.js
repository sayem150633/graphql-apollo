const User = require("../../models/User");
const { ApolloError } = require('apollo-server-errors')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

module.exports = {
  Mutation: {
    async registerUser(_, { registerInput: { username, email, password } }) {
      // see old user registration
      const oldUser = await User.findOne({ email });

      // throw errors
      if (oldUser) {
        throw new ApolloError(
          "A user is already registered with this email " + email,
          "USER_ALREADY_EXISTS"
        );
      }

      // encrypt password
      let encryptedPassword = await bcrypt.hash(password, 10);

      // build out models
      const newUser = new User({
        username: username,
        email: email.toLowerCase(),
        password: encryptedPassword,
      });

      // create jwt token
      const token = jwt.sign({ user_id: newUser._id, email }, "UNSAFE_STRING", {
        expiresIn: "2h",
      });
      newUser.token = token;
      // save user in mongodb
      const res = await newUser.save();
      return {
        id: res.id,
        ...res._doc,
      };
    },
    async loginUser(_, { loginInput: { email, password } }) {
      // see user exists
      const user = await User.findOne({ email });

      // create password is valid
      if (user && (await bcrypt.compare(password, user.password))) {
        // create a token
        const token = jwt.sign({ user_id: user._id, email }, "UNSAFE_STRING", {
          expiresIn: "2h",
        });
        // attach token to user model
        user.token = token;
        return {
          id: user.id,
          ...user._doc,
        };
      } else {
        // throw exception
        throw new ApolloError("Incorrect password", "INCORRECT_PASSWORD");
      }
    },
  },
  Query: {
    // user: async (_, args) => await User.findOne(args),
    user: async (_, { id }) => {
      console.log('id', id);
       return await User.findById(id);
    },
    users: async() => await User.find()
  },
};