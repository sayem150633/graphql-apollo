const { AuthenticationError } = require('apollo-server')

const jwt = require('jsonwebtoken')

module.exports = (context) => {
    // headers
    const authHeader = context.req.headers.authorization;
    if (authHeader) {
        // Bearer token
        const token = authHeader.split('Bearer')[1]
        if (token) {
            try {
                const user = jwt.verify(token, "UNSAFE_STRING");
                return user
            } catch (e) { 
                throw new AuthenticationError("Invalid/Expired token")
            }
        }
        throw new Error("Invalid Bearer token")
    }
    throw new Error("Authorization header must be provided")
}
