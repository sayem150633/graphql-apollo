import React, { useContext } from 'react'
import { AuthContext } from '../context/authContext';

const Home = () => {
    const {user} = useContext(AuthContext);
  return (
      <div>{user ? <h1>{ user?.email}</h1> : <h1>Public page</h1>}</div>
  )
}

export default Home