import React, { useContext, useState } from 'react'
import {  gql, useMutation } from "@apollo/client";
import { AuthContext } from '../context/authContext';
import { useNavigate } from 'react-router-dom';
import { useForm } from '../utility/hooks';
import { Alert, Button, Container, Stack, TextField } from '@mui/material';

const REGISTER_USER = gql`
  mutation RegisterUser($registerInput: RegisterInput) {
    registerUser(registerInput: $registerInput) {
      username
      email
      password
      token
    }
  }
`;

const Register = () => {
    const context = useContext(AuthContext)
    let navigate = useNavigate()
    const [errors, setErrors] = useState([])
    function registerUserCallback() {
        console.log("call back");
        registerUser()
    }
    const { onChange, onSubmit, values } = useForm(registerUserCallback, {
        username: "",
        email: "",
        password: "",
        confirmPassword: "",
    })
    const [registerUser, { loading }] = useMutation(REGISTER_USER, {
        update(proxy, { data: { registerUser: userData } }) {
            context.login(userData)
            navigate("/")
        },
        onError({ graphQLErrors }) {
            setErrors(graphQLErrors)
        },
        variables: {registerInput: values}
    });
  return (
    <Container maxWidth="sm">
      <h1>Register</h1>
      <p>This is the register page</p>
      <Stack spacing={2} paddingBottom={2}>
        <TextField label="username" name="username" onChange={onChange} />
        <TextField label="email" name="email" onChange={onChange} />
        <TextField label="password" name="password" onChange={onChange} />
        <TextField
          label="confirmPassword"
          name="confirmPassword"
          onChange={onChange}
        />
      </Stack>
      {errors?.map((err, index) => (
        <Alert severity="error" key={index}>
          {err.message}
        </Alert>
      ))}
          <Button variant='contained' onClick={onSubmit} >Register </Button>
    </Container>
  );
}

export default Register