import React, { useContext, useState } from "react";
import { gql, useMutation } from "@apollo/client";
import { AuthContext } from "../context/authContext";
import { useNavigate } from "react-router-dom";
import { useForm } from "../utility/hooks";
import { Alert, Button, Container, Stack, TextField } from "@mui/material";

const LOGIN_USER = gql`
  mutation LoginUser($loginInput: LoginInput) {
    loginUser(loginInput: $loginInput) {
      username
      email
      password
      token
    }
  }
`;

const Login = () => {
  const context = useContext(AuthContext);
  let navigate = useNavigate();
  const [errors, setErrors] = useState([]);
  function loginUserCallback() {
    console.log("call back");
    loginUser();
  }
  const { onChange, onSubmit, values } = useForm(loginUserCallback, {
    email: "",
    password: "",
  });
  const [loginUser, { loading }] = useMutation(LOGIN_USER, {
    update(proxy, { data: { loginUser: userData } }) {
      context.login(userData);
      navigate("/");
    },
    onError({ graphQLErrors }) {
      setErrors(graphQLErrors);
    },
    variables: { loginInput: values },
  });
  return (
    <Container maxWidth="sm">
      <h1>Login</h1>
      <p>This is the login page</p>
      <Stack spacing={2} paddingBottom={2}>
        <TextField label="email" name="email" onChange={onChange} />
        <TextField label="password" name="password" type="password" onChange={onChange} />
      </Stack>
      {errors?.map((err, index) => (
        <Alert severity="error" key={index}>
          {err.message}
        </Alert>
      ))}
      <Button variant="contained" onClick={onSubmit}>
        Login{" "}
      </Button>
    </Container>
  );
};

export default Login;
