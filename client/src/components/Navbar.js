import React, { useContext } from "react";
import { AppBar, Box, Button, Toolbar, Typography } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { AuthContext } from "../context/authContext";

const Navbar = () => {
  const { user, logout } = useContext(AuthContext);
  console.log(user);
  const navigate = useNavigate();
  const onLogout = () => {
    logout();
    navigate("/login");
  };
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h5" component="div">
            <Link
              to={"/"}
              style={{
                textDecoration: "none",
                color: "white",
                marginRight: "10px",
              }}
            >
              React
            </Link>
          </Typography>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            News
          </Typography>
          {user ? (
            <>
              <Button variant="contained" onClick={logout}>
                Logout
              </Button>
            </>
          ) : (
            <>
              <Link
                to={"/login"}
                style={{
                  textDecoration: "none",
                  color: "white",
                  marginRight: "5px",
                }}
              >
                Login
              </Link>
              <Link
                to={"/register"}
                style={{ textDecoration: "none", color: "white" }}
              >
                Register
              </Link>
            </>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Navbar;
